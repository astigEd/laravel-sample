<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRatingRequest;
use App\Http\Resources\ProductRatingResource;
use App\Models\Rating;

class ProductRatingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ratings = Rating::all();
        return $this->success(ProductRatingResource::collection($ratings), 'Products ratings retrieved successfully.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductRatingRequest $request)
    {
        $input = $request->validated(); 
        $input['user_id'] = auth()->id();
        $input['status'] = 1;
       
        $rate = Rating::create($input);
        Log::info("Rating {$rate->rating} for Product ID {$request->product_id}.");
        
        return $this->success(new ProductRatingResource($rate->fresh()), 'Product rating successfully submitted.', Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Rating $rating) 
    {
        return $this->success(new ProductRatingResource($rating), 'Product rating successfully retrieved.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductRatingRequest $request, Rating $rating)
    {
        $rating->update($request->validated());

        Log::info("Rating ID {$rating->id} updated successfully.");
        
        return $this->success(new ProductRatingResource($rating->fresh()), 'Product rating successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rating $rating)
    {
        $rating->delete();

        Log::info("Product rating ID {$rating->id} deleted successfully.");

        return $this->success(null , 'Product rating successfully deleted.');
    }
}
