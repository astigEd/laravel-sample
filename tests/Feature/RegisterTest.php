<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    public function testsRegistersSuccessfully()
    {
        $payload = [
            'name' => 'Jose',
            'email' => 'jose@test.com',
            'password' => 'test123',
            'confirm_password' => 'test123',
        ];

        $this->json('post', '/api/register', $payload)
            ->assertStatus(201)
            ->assertJsonStructure([
                'success',
                'message',
                'data' => [
                    'user' => [
                        'id',
                        'name',
                        'email',
                        'created_at',
                        'updated_at'
                    ],
                    'token'
                ],
            ]);;
    }

    public function testsRequiresPasswordEmailAndName()
    {
        $this->json('post', '/api/register')
            ->assertStatus(400)
            ->assertJson([
                'success' => false, 
                'message' => 'Validation error.',
                'data' => [
                    'name' => ['The name field is required.'],
                    'email' => ['The email field is required.'],
                    'password' => ['The password field is required.'],
                    'confirm_password' => ['The confirm password field is required.'],
                ]
            ]);
    }

    public function testsMatchPasswordConfirmation()
    {
        $payload = [
            'name' => 'Jose',
            'email' => 'jose@test.com',
            'password' => 'test123',
            'confirm_password' => 'test123t',
        ];

        $this->json('post', '/api/register', $payload)
            ->assertStatus(400)
            ->assertJson([
                'success' => false, 
                'message' => 'Validation error.',
                'data' => [
                    'confirm_password' => ['The confirm password and password must match.'],
                ]
            ]);
    }
}
